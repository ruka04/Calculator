<?php
// ex1 中級 Q1 計算機

// 四則演算ができること 
// ＝+ - * /　が入る 　＊１

// 負の値も計算できること  
// 値がマイナスでも行けたのでOK

// 入力値：数値2つと計算符号を元に計算結果を出力すること
// 数字A ＊１　数字B　という形
// 入力値例：2 × 3
// 結果：6

// 1.できるだけ条件のネストを減らす
// 2.エラーは書けるなら正常系の前処理で判定

class Calculater {
    public static function calc($int1, $oprator, $int2) {
        try {
            //ネストが増えるとわかりづらくなるので先にエラー条件を書く
            if (!is_numeric($int1) || !is_numeric($int2)) {
                throw new Exception('エラー：整数を入力してください！');
            }
            if ($int2 == 0 && $oprator == "/") {
                throw new Exception('エラー：0では割れません！');
            }
            //ここからは正常系
            if ($oprator == "+"){
                return $int1 + $int2;
            } else if($oprator == "-") {
                return $int1 - $int2;
            } else if ($oprator == "*") {
                return $int1 * $int2;
            } else if ($oprator == "/") {
                return $int1 / $int2;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

?>